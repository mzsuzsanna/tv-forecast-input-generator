# TV Weather Forecast Input Generator

+ The programs is here to help a local tv channel extract and import data for weather forecast.
+ It has no graphical interface.
+ It prints into xlsx files the extracted information from Accuweather API, previously knowing the desired towns.
+ The .py file can be converted to .exe using the auto-py-to-exe library (https://pypi.org/project/auto-py-to-exe/).
