# cmd: auto-py-to-exe
# Console based, One file
# 4 Days of Daily Forecast, each day printed in different output file
# Resource URL:
# "http://dataservice.accuweather.com/forecasts/v1/daily/5day/{locationsapi}?apikey={
# apikey}&language=ro&details=false&metric=true"

import os
import requests

from constants import CITIES, DATUM_FORMAT, URL_FORMAT, OUTPUT_TXT_NAME_FORMAT
from api_key_operations import get_api_key

api_key = get_api_key()
# a different file descriptor for each day
file_list = []
response = requests.get(URL_FORMAT.format(CITIES[0][1], api_key))
if response.status_code == 200:  # if there's no response error
    r = response.json()
    for nr_day in range(len(file_list)):
        # Starting from 1, not 0, because today's forecast needs to be skipped over
        date = r["DailyForecasts"][1]["Date"]
        year = date[0:4]
        month = date[5:7]
        day = date[8:10]
        first_line = DATUM_FORMAT.format(year, month, day)

        output_file_name = OUTPUT_TXT_NAME_FORMAT.format(nr_day + 1)
        if os.path.exists(output_file_name):
            os.remove(output_file_name)
        else:
            f = open(output_file_name, "x")
            f.close()

        # create file descriptor for each day separately
        day_file = open(output_file_name, "w")
        day_file.write(first_line)
        file_list.append(day_file)
else:
    print("Response Error")

for city_nr in range(1, len(CITIES)):
    response = requests.get(URL_FORMAT.format(CITIES[city_nr][1], api_key))
    if response.status_code == 200:  # if there's no response error
        r = response.json()
        for nr_day in range(len(file_list)):
            max_temp = round(r["DailyForecasts"][nr_day + 1]["Temperature"]["Maximum"]["Value"])
            min_temp = round(r["DailyForecasts"][nr_day + 1]["Temperature"]["Minimum"]["Value"])
            icon = r["DailyForecasts"][nr_day + 1]["Day"]["Icon"]
            file_list[nr_day].write(
                "varos{}=[\"{}\",\"{}°\",\"/{}°\",\"{}\"]\n".format(city_nr, CITIES[city_nr][0], max_temp, min_temp,
                                                                    icon))
    else:
        print("Response Error")


# close files
for day_file in file_list:
    day_file.close()

close = input(">>> Kilépéshez Enter <<<")
