# save forecast results to xls file with cmd: auto-py-to-exe
# Console based, One file
# 1 Day of Daily Weather Forecasts, printed into a xlsx file
# Resource URL: http://dataservice.accuweather.com/forecasts/v1/daily/1day/{locationsapi}?api_key={api_key}

import requests
import xlsxwriter

from constants import CITIES, URL_FORMAT, PATH_FORMAT, OUTPUT_XLS_NAME_FORMAT
from api_key_operations import get_api_key


api_key = get_api_key()

# xlsx
file = xlsxwriter.Workbook(OUTPUT_XLS_NAME_FORMAT)
worksheet = file.add_worksheet()
row = 0
column = 0
worksheet.set_column(0, 0, 20)
worksheet.set_column(3, 3, 60)

for city_nr in range(len(CITIES)):
    response = requests.get(URL_FORMAT.format(CITIES[city_nr][1], api_key))
    if response.status_code == 200:
        r = response.json()
        max_temp = round(r["DailyForecasts"][0]["Temperature"]["Maximum"]["Value"])
        min_temp = round(r["DailyForecasts"][0]["Temperature"]["Minimum"]["Value"])
        icon = r["DailyForecasts"][0]["Day"]["Icon"]
        worksheet.write(row, column, CITIES[city_nr][0])
        worksheet.write(row, column + 1, max_temp)
        worksheet.write(row, column + 2, min_temp)
        new_path = PATH_FORMAT.format(icon)
        worksheet.write(row, column + 3, new_path)
        row += 1

    else:
        print("Response Error")

close = input(">>> Kilépéshez Enter <<<")
file.close()
