API_FILE_NAME = "..\\data\\api_keys.txt"

# Locations API
# Arad,Beszterce, Brassó, Budapest, Bukarest, Csíkszereda, Déva, Gyergyószentmiklós, Kézdivásárhely, Kolozsvár,
# Marosvásárhely, Nagybánya Nagybánya, Nagyszeben, Nagyvárad, Sepsiszentgyörgy, Szatmárnémeti, Székelyudvarhely,
# Temesvár
# city["City name", Locations API]
CITIES = [["Arad", 286942], ["Beszterce", 272286], ["Brassó", 287345], ["Budapest", 187423], ["Bukarest", 287430],
        ["Csíkszereda", 273485], ["Déva", 273576], ["Gyergyószentmiklós", 273486], ["Kézdivásárhely", 272906],
        ["Kolozsvár", 287713], ["Marosvásárhely", 289415], ["Nagybánya", 274475], ["Nagyszeben", 290499],
        ["Nagyvárad", 287292], ["Sepsiszentgyörgy", 272904], ["Szatmárnémeti", 275551],
        ["Székelyudvarhely", 273487], ["Temesvár", 290867]]

DATA_FORMAT = "{}. {}. {}."
DATUM_FORMAT = "datum=[\"{}. {}. {}.\"]\n"
OUTPUT_TXT_NAME_FORMAT = "idojaras{}.txt"
OUTPUT_XLS_NAME_FORMAT = "idojaras.xlsx"
PATH_FORMAT = r"\\ST1\NewsRoom\GraphicModels\Mozaik\Kellekek\{}.png "
URL_FORMAT = ("http://dataservice.accuweather.com/forecasts/v1/daily/5day/{}?apikey={"
              "}&language=ro&details=false&metric=true")
