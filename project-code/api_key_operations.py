import os
import sys
import time

from constants import API_FILE_NAME


def get_api_key():
    nr_keys = 0  # number of api keys
    nr_used_twice = 0  # number of api keys used already twice
    api_keys = []  # array of api keys
    number = []
    # read all the api keys from the file
    if os.path.exists(API_FILE_NAME):
        f = open(API_FILE_NAME, "r")
        for line in f:
            api_keys.append(line[0:32])
            number.append(int(line[33:34]))
            nr_keys += 1
            if number[nr_keys - 1] == 2:
                nr_used_twice += 1
        f.close()
    else:
        print(f"Nincs az aktuális katalógusban {API_FILE_NAME}\n")
        time.sleep(5)
        sys.exit()

    if nr_used_twice != nr_keys:  # if the last was already used twice
        api_key = api_keys[nr_used_twice]
    else:
        api_key = api_keys[0]

    # Api key-file update
    os.remove(API_FILE_NAME)
    g = open(API_FILE_NAME, 'w')
    for i in range(nr_keys):
        line = api_keys[i] + ' '
        if i == nr_used_twice:
            number[i] += 1

        if nr_used_twice == 16:
            if i == 0:
                line = line + "1"
            else:
                line = line + "0"
        else:
            line = line + str(number[i])

        g.write(line)
        g.write('\n')

    g.close()

    return api_key
